# frozen_string_literal: true

class ApplicationController < ActionController::Base
  helper_method :current_user

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def require_user
    unless current_user.present?
      flash[:notice] = 'You must be logged in to perform that action!'
      redirect_to root_path
    end
  end

  def require_admin
    if !current_user.present? || !current_user&.admin?
      flash[:notice] = 'Only admin users can perform that action'
      redirect_to categories_path
    end
  end

  def require_same_user(object)
    if User.find(session[:user_id]) != object && !current_user.admin?
      flash[:notice] = 'You are not a valid user to perform that action'
      redirect_to root_path
    end
  end
end
