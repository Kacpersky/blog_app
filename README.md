## Description of blog_app
blog_app - it's a blog for users containing CRUD functions, multiple resources, authentication system built from scratch, one-to-many and many-to-many associations at DB layer, rspec tests, front-end using Bootstrap.

## Code style

[![rubocop](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/rubocop-hq/rubocop)

## Screenshots

![welcome_page](https://i.imgur.com/jghKpUa.png)

## Built with:

- [Ruby 2.5.1](https://www.ruby-lang.org/en/)
- [Ruby on Rails 5](https://rubyonrails.org/)
- [Bootstrap 3](https://getbootstrap.com/)

## Features

- sign up
- create articles and tie them by categories
- read other user's articles
- read various articles related to specific categories

## Tests

To run tests you need to clone my app, install [rspec](https://github.com/rspec/rspec-rails) and run in terminal:

```sh
rspec spec
```

## How to use?

Visit [my blog](https://mekarski-blog-app.heroku.com/) and share your articles with rest of the world!

## Authors

* **Kacper Mekarski** - *Initial work* - [Kacper Mekarski](https://gitlab.com/Kacpersky)

See also the list of [team](https://gitlab.com/Kacpersky/blog_app/activity) who participate in this project.

## License

MIT © [Kacper Mękarski]()
