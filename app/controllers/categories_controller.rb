# frozen_string_literal: true

class CategoriesController < ApplicationController
  before_action :require_admin, except: %i[index show]
  before_action :find_params, only: %i[show edit update]

  def index
    @categories = Category.paginate(page: params[:page], per_page: 5)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(categories_params)
    if @category.save
      flash[:notice] = 'Category was succesfully created'
      redirect_to categories_path
    else
      render 'new'
    end
  end

  def show
    @category_articles = @category.articles.paginate(page: params[:page], per_page: 5)
  end

  def edit; end

  def update
    if @category.update(category_params)
      flash[:notice] = 'Category name was succesfully updated'
      redirect_to category_path(@category)
    else
      render 'edit'
    end
  end

  private

  def categories_params
    params.require(:category).permit(:name)
  end

  def find_params
    @category = Category.find(params[:id])
  end
end
