# frozen_string_literal: true

class User < ActiveRecord::Base
  has_many :articles, dependent: :delete_all
  validates :username, presence: true, uniqueness: { case_sensitive: false },
                       length: { minimum: 3, maximum: 25 }
  validates :email, presence: true, uniqueness: { case_sensitive: false },
                    format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, presence: true
  has_secure_password
end
